<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Painel de controle</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-4 bg-dark ">
                <form id="formulario" action="ajax/upload.php" method="post" enctype="multipart/form-data">
                <div id="mensagem"></div>
                    <input type="file" name="imagens" id="imagens" placehoder="insira sua imagem aqui">
                    <input type="submit" value="Salvar" id="salvar" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        $(#formulario).on('submit', function(){
            var formulario = $(this);
            var botao = $('#salvar');
            var mensagem = $('#mensagem');

            botao.button('carregando');

            $(this).ajaxSubmmit({
                dataType:'json',
                sucess: function(returno){
                    mensagem.attr('class','alert alert-sucess');

                    formulario.resetForm();
                }else{
                    mensagem.attr('class','alert alert-danger')
                }
            });
        });
    </script>
</body>
</html>